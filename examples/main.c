#include "http_server.h"
#include "middlewares.h"

// Middleware de log
void logging_middleware(int client_socket, HttpParsed* http_parsed) {
    // Lógica de log, por exemplo, registrar informações sobre a solicitação
    printf("Log: %s %s\n", http_parsed->method, http_parsed->resource);
}

// Middleware de autenticação
void authentication_middleware(int client_socket, HttpParsed* http_parsed) {
    // Lógica de autenticação, por exemplo, verificar tokens, cookies, etc.
    printf("Autenticação realizada com sucesso para a rota %s\n", http_parsed->resource);
}

void home_handler(int client_socket, const char* request_path) {
    dprintf(client_socket, "HTTP/1.1 200 OK\r\nContent-Type: text/html\r\n\r\n<html><body>Welcome to the Home Page!</body></html>");
}

void about_handler(int client_socket, const char* request_path) {
    dprintf(client_socket, "HTTP/1.1 200 OK\r\nContent-Type: text/html\r\n\r\n<html><body>About Us Page</body></html>");
}

void not_found_handler(int client_socket, const char* request_path) {
    dprintf(client_socket, "HTTP/1.1 404 Not Found\r\nContent-Type: text/html\r\n\r\n<html><body>404 - Not Found: %s</body></html>", request_path);
}

int main() {
    Middleware general_middlewares[] = {logging_middleware, NULL};
    Middleware index_middlewares[] = {authentication_middleware, NULL};

    // Define routes
    Route routes[] = {
        {"/", "GET", home_handler, index_middlewares},
        {"/about", "GET", about_handler, NULL},
        {NULL, NULL, NULL, NULL}  // End of routes marker
    };

    // Define server configuration
    ServerConfig server_config = {
        .port = 8080,
        .allowed_hosts = "localhost",
        .routes = routes,
        .not_found_handler = not_found_handler,
        .middlewares = general_middlewares
    };

    // Start the server
    start_server(&server_config);

    return 0;
}

