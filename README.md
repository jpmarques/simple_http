# Simple HTTP

> Simple HTTP é uma biblioteca simples desenvolvida para estudos, projetada para facilitar o entendimento de conceitos essenciais no desenvolvimento web. Com funcionalidades básicas para operações HTTP, GET, POST, caching e gestão de sessões, esta biblioteca é ideal para quem está começando a explorar o mundo do desenvolvimento web e deseja aprimorar suas habilidades de programação.

### Ajustes e melhorias

O projeto ainda está em desenvolvimento e as próximas atualizações serão voltadas nas seguintes tarefas:

- [x] Capacidade de lidar com requisições simples (GET).
- [x] Capacidade de lidar com Roteamento.
- [x] Criar um parser para a requisicao.
- [x] Capacidade de lidar com outros tipos de requisicoes (GET, POST, PUT, ...).
- [x] suporte a middlewares.
- [ ] Gestão simples de sessões para armazenar dados temporários.
- [ ] Mapeamento de Diretórios Estáticos (html/css).

<!---
## 💻 Pré-requisitos

Antes de começar, verifique se você atendeu aos seguintes requisitos:

- Você instalou a versão mais recente de `<linguagem / dependência / requeridos>`
- Você tem uma máquina `<Windows / Linux / Mac>`. Indique qual sistema operacional é compatível / não compatível.
- Você leu `<guia / link / documentação_relacionada_ao_projeto>`.

## 🚀 Instalando <nome_do_projeto>

Para instalar o <nome_do_projeto>, siga estas etapas:

Linux e macOS:

```
<comando_de_instalação>
```

Windows:

```
<comando_de_instalação>
```
-->
## ☕ Usando SimpleHTTP

exemplo de uso da biblioteca

importando a biblioteca e criando um main.c
```
#include "http_server.h"
#include "middlewares.h"

// Middleware de log
void logging_middleware(int client_socket, HttpParsed* http_parsed) {
    // Lógica de log, por exemplo, registrar informações sobre a solicitação
    printf("Log: %s %s\n", http_parsed->method, http_parsed->resource);
}

// Middleware de autenticação
void authentication_middleware(int client_socket, HttpParsed* http_parsed) {
    // Lógica de autenticação, por exemplo, verificar tokens, cookies, etc.
    printf("Autenticação realizada com sucesso para a rota %s\n", http_parsed->resource);
}

void home_handler(int client_socket, const char* request_path) {
    dprintf(client_socket, "HTTP/1.1 200 OK\r\nContent-Type: text/html\r\n\r\n<html><body>Welcome to the Home Page!</body></html>");
}

void about_handler(int client_socket, const char* request_path) {
    dprintf(client_socket, "HTTP/1.1 200 OK\r\nContent-Type: text/html\r\n\r\n<html><body>About Us Page</body></html>");
}

void not_found_handler(int client_socket, const char* request_path) {
    dprintf(client_socket, "HTTP/1.1 404 Not Found\r\nContent-Type: text/html\r\n\r\n<html><body>404 - Not Found: %s</body></html>", request_path);
}

int main() {
    Middleware general_middlewares[] = {logging_middleware, NULL};
    Middleware index_middlewares[] = {authentication_middleware, NULL};

    // Define routes
    Route routes[] = {
        {"/", "GET", home_handler, index_middlewares},
        {"/about", "GET", about_handler, NULL},
        {NULL, NULL, NULL, NULL}  // End of routes marker
    };

    // Define server configuration
    ServerConfig server_config = {
        .port = 8080,
        .allowed_hosts = "localhost",
        .routes = routes,
        .not_found_handler = not_found_handler,
        .middlewares = general_middlewares
    };

    // Start the server
    start_server(&server_config);

    return 0;
}

```
compilando e executando o projeto
```
gcc -I./include examples/main.c src/http_parser src/http_server.c -o main && ./main
```
testando index page com o curl
```
curl http://localhost:8080
saida -> <html><body>Welcome to the Home Page!</body></html>
```
testando about page com o curl
```
curl http://localhost:8080/about
saida -> <html><body>About Us Page</body></html>
```
testando caminho nao especificado com o curl
```
curl http://localhost:8080/aleatorio
<html><body>404 - Not Found: /aleatorio</body></html>
```

Adicione comandos de execução e exemplos que você acha que os usuários acharão úteis. Fornece uma referência de opções para pontos de bônus!

## 📫 Contribuindo para Simple HTTP

Para contribuir com simple HTTP, siga estas etapas:

1. Bifurque este repositório.
2. Crie um branch: `git checkout -b <nome_branch>`.
3. Faça suas alterações e confirme-as: `git commit -m '<mensagem_commit>'`
4. Envie para o branch original: `git push origin <nome_do_projeto> / <local>`
5. Crie a solicitação de pull.

Como alternativa, consulte a documentação do Codeberg em [como criar uma solicitação pull](https://docs.codeberg.org/collaborating/pull-requests-and-git-flow/).

## 🤝 Colaboradores

Agradecemos às seguintes pessoas que contribuíram para este projeto:

<table>
  <tr>
    <td align="center">
      <a href="https://codeberg.org/jpmarques" title="perfil do codeberg">
        <img src="https://codeberg.org/avatars/8e446c80eb92f6e8db21b516df225d174cd1dab3d812b4a9fdb27926b329bde0?size=512" width="100px;" alt="Foto do Joao no Codeberg"/><br>
        <sub>
          <b>jpmarques</b>
        </sub>
      </a>
    </td>
  </tr>
</table>
<!--
## 😄 Seja um dos contribuidores

Quer fazer parte desse projeto? Clique [AQUI](CONTRIBUTING.md) e leia como contribuir.
-->
## 📝 Licença

Esse projeto está sob licença. Veja o arquivo [LICENÇA](LICENSE.md) para mais detalhes.

