#ifndef HTTP_SERVER_H
#define HTTP_SERVER_H

#include <stdio.h>
#include "middlewares.h"
// Function pointer type for route handlers
typedef void (*RouteHandler)(int client_socket, const char* request_path);

typedef struct {
    const char* path;
    const char* method;
    RouteHandler handler;
    Middleware* middlewares;
} Route;

typedef struct {
    int port;
    const char* allowed_hosts;
    const Route* routes;  // Array of route definitions
    RouteHandler not_found_handler;
    Middleware* middlewares;
} ServerConfig;

void start_server(const ServerConfig* server_config);

#endif  

