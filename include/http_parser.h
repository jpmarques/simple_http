#ifndef HTTP_PARSER_H
#define HTTP_PARSER_H

typedef struct {
    char method[16];
    char resource[256];
    char http_version[16];
    char** headers;
    int headers_len;
    char* body;
} HttpParsed;

HttpParsed parse_http(char* http_request);
void free_http_parsed(HttpParsed* http_parsed);

#endif  // HTTP_PARSER_H

