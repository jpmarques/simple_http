#include "http_parser.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void parse_request_line(const char* http_request, HttpParsed* request_line) {
    sscanf(http_request, "%s %s %s", request_line->method, request_line->resource, request_line->http_version);
}

void str_split(const char* str, const char* delim, char*** resultado, int* i) {
    size_t tamanho_original = strlen(str) + 1; 
    
    char *str_cp = malloc(tamanho_original);
    strcpy(str_cp, str);
    char *ptr = strtok(str_cp, delim);
    *i = 0;

    // Aloca memória para o array de ponteiros
    *resultado = malloc(tamanho_original * sizeof(char*));

    while(ptr != NULL){
        (*resultado)[*i] = ptr;
		ptr = strtok(NULL, delim);
        (*i)++;
	}
    free(str_cp);
}

HttpParsed parse_http(char* http_request) {
    HttpParsed http_parsed;
    parse_request_line(http_request, &http_parsed);
    char** resultado;
    int tamanho;
    str_split(http_request, "\r\n", &resultado, &tamanho);

    // Restante do código...
    
    // Dynamically allocate memory for headers
    http_parsed.headers = malloc((tamanho - 1) * sizeof(char*));

    for (int i = 1; i < tamanho - 1; i++) {
        http_parsed.headers[i - 1] = resultado[i];
    }
    http_parsed.headers[tamanho - 2] = NULL;
    http_parsed.headers_len = tamanho - 1;
    http_parsed.body = resultado[tamanho - 1];

    free(resultado);  // Libera a memória alocada para resultado

    return http_parsed;
}

// Free memory allocated for headers
void free_http_parsed(HttpParsed* http_parsed) {
    free(http_parsed->headers);
}
