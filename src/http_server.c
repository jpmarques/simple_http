// http_server.c

#include "http_server.h"
#include "http_parser.h"
#include "middlewares.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/types.h>

#define BUFFER_SIZE 1024

void execute_middlewares(Middleware* middlewares, int client_socket, HttpParsed* http_parsed) {
    // Executa cada middleware associado à rota
    if (middlewares != NULL) {
        for (int i = 0; middlewares[i] != NULL; ++i) {
            middlewares[i](client_socket, http_parsed);
        }
    }
}

void handle_request(int client_socket, HttpParsed* http_parsed, const Route* routes, RouteHandler not_found_handler, Middleware* geral_middlewares) {
    // Executa middlewares gerais
    execute_middlewares(geral_middlewares, client_socket, http_parsed);

    if (routes == NULL) {
        // No routes defined, call the not found handler
        if (not_found_handler != NULL) {
            not_found_handler(client_socket, http_parsed->resource);
        } else {
            dprintf(client_socket, "HTTP/1.1 404 Not Found\r\nContent-Type: text/html\r\n\r\n<html><body>404 - Not Found</body></html>");
        }
    } else {
        for (const Route* route = routes; route->path != NULL; ++route) {
            if (strcmp(http_parsed->resource, route->path) == 0 && strcmp(http_parsed->method, route->method)==0){
                // Executa middlewares especificos
                execute_middlewares(route->middlewares, client_socket, http_parsed);
                // Found a matching route, call the handler
                route->handler(client_socket, http_parsed->resource);
                return;
            }
        }

        // No matching route found, call the not found handler
        if (not_found_handler != NULL) {
            not_found_handler(client_socket, http_parsed->resource);
        } else {
            dprintf(client_socket, "HTTP/1.1 404 Not Found\r\nContent-Type: text/html\r\n\r\n<html><body>404 - Not Found</body></html>");
        }
    }
}

void start_server(const ServerConfig* server_config) {
    int server_fd, new_socket;
    struct sockaddr_in address;
    int addrlen = sizeof(address);

    // Create socket
    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
        perror("Socket creation failed");
        exit(EXIT_FAILURE);
    }

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(server_config->port);

    // Bind the socket
    if (bind(server_fd, (struct sockaddr*)&address, sizeof(address)) < 0) {
        perror("Bind failed");
        exit(EXIT_FAILURE);
    }

    // Listen for incoming connections
    if (listen(server_fd, 3) < 0) {
        perror("Listen failed");
        exit(EXIT_FAILURE);
    }

    printf("Server listening on port %d...\n", server_config->port);

    while (1) {
        // Accept incoming connection
        if ((new_socket = accept(server_fd, (struct sockaddr*)&address, (socklen_t*)&addrlen)) < 0) {
            perror("Accept failed");
            exit(EXIT_FAILURE);
        }

        // Receive the HTTP request
        char buffer[BUFFER_SIZE] = {0};
        ssize_t bytes_received = read(new_socket, buffer, BUFFER_SIZE);

        if (bytes_received > 0) {
            HttpParsed http_parsed = parse_http(buffer);
            // Handle the request
            handle_request(new_socket, &http_parsed, server_config->routes, server_config->not_found_handler, server_config->middlewares);
        }

        // Close the socket after handling the request
        close(new_socket);
    }

}

